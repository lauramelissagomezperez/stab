<?php
	require_once('../Modelo/Apartamento.php');
	require_once('../Modelo/CrudApartamento.php');

	$apartamento = new apartamento();
	$CrudApartamento = new CrudApartamento();
	if (isset($_POST["Registrar"])) {
		//echo "Registrar";
		//Instanciar los atributos
		$apartamento->setNApartamento($_POST["NApartamento"]);
		$apartamento->setParqueadero($_POST["Parqueadero"]);
		if ( isset($_POST["CuartoUtil"]) ){
			$apartamento->setCuartoUtil($_POST['CuartoUtil']);
		}
		$apartamento->setIdEstado($_POST['IdEstado']);
        $apartamento->setMetrosCuadrados($_POST['MetrosCuadrados']);
        $apartamento->setValorMetrosCuadrados($_POST['ValorMetrosCuadrados']);
        $apartamento->setValorParqueadero($_POST['ValorParqueadero']);
        $apartamento->setValorCuartoUtil($_POST['ValorCuartoUtil']);         
			$CrudApartamento::IngresarApartamento($apartamento);
	}
	elseif(isset($_POST["Modificar"])){ //Si la peticion es de modificar

    	//echo"Modificar";
    	//Instanciar los atributos
    	$apartamento->setNApartamento($_POST["NApartamento"]);
		$apartamento->setParqueadero($_POST["Parqueadero"]);
		$apartamento->setCuartoUtil($_POST['CuartoUtil']);
		$apartamento->setIdEstado($_POST['IdEstado']);
        $apartamento->setMetrosCuadrados($_POST['MetrosCuadrados']);
        $apartamento->setValorMetrosCuadrados($_POST['ValorMetrosCuadrados']);
        $apartamento->setValorParqueadero($_POST['ValorParqueadero']);
        $apartamento->setValorCuartoUtil($_POST['ValorCuartoUtil']);
		
		$CrudApartamento::ModificarApartamento($apartamento);			
	}
	else if($_GET['Accion']=="CambiarDeEstado")
	{
  		$CrudApartamento::CambiarDeEstado($_GET["NApartamento"]);
	} 
	else if($_GET['Accion']=="CambiarDeEstadoActivo")
	{
  		$CrudApartamento::CambiarDeEstadoActivo($_GET["NApartamento"]);
	}
?>