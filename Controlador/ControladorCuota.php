<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Generar</title>
</head>
<body>
    
<?php
require_once('../Modelo/Pago.php');
require_once('../Modelo/CrudPago.php');
$Pago = new Pago();
$CrudPago = new CrudPago();
if(isset($_POST["Registrar"])) // Si la peticion es registrar
{
    //Instanciar los atributos
    $Pago->setNCuentaCobro($_POST["NcuentaCobro"]);
    $Pago->setPeriodo($_POST["Periodo"]);
    $Pago->setFecha($_POST["FechaActual"]);
    $Pago->setFechaLimite($_POST["FechaLimite"]);
    $Pago->setNapartamento($_POST["NApartamento"]);
    $Pago->setPropietario($_POST["Propietario"]);
    $Pago->setDireccionEntrega($_POST["DireccionEntrega"]);
    $Pago->setCorreo($_POST["Email"]);
    $Pago->setValorParqueadero($_POST["Parqueadero"]);
    $Pago->setCuartoUtil($_POST["Cuarto"]);
    $Pago->setM2($_POST["M2"]);
    $Pago->setValorMulta($_POST["ValorMulta"]);
    $Pago->setSaldoaFavor($_POST["SaldoaFavor"]);
    $Pago->setTotalPagar($_POST["TotalPagar"]);
    $Pago->setTipoPago($_POST["TipoPago"]);
    $Pago->setIdEstado($_POST["IdEstado"]);
    $Pago->setObservaciones($_POST["Observaciones"]);
   
    $CrudPago::InsertarPago($Pago); //Llamar el metodo para insertar
    
}


else if($_GET['Accion']=="EliminarPago")
    {
    $CrudPago::EliminarPago($_GET["NCuentaCobro"]);

    }

?>

</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>