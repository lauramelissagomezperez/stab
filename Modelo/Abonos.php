<?php

class Abono{
private $IdAbono;
private $Estado;
private $IdPago;
private $FechaPago;
private $ValorPago;


public function __construct(){}

public function setIdAbono($IdAbono){
    $this->IdAbono = $IdAbono;
}
public function getIdAbono()
{
    return $this->IdAbono;
}

public function setEstado($Estado){
    $this->Estado = $Estado;
}
public function getEstado()
{
    return $this->Estado;
}

public function setIdPago($IdPago){
    $this->IdPago = $IdPago;
}
public function getIdPago()
{
    return $this->IdPago;
}

public function setFechaPago($FechaPago){
    $this->FechaPago = $FechaPago;
}
public function getFechaPago()
{
    return $this->FechaPago;
}

public function setValorPago($ValorPago){
    $this->ValorPago = $ValorPago;
}
public function getValorPago()
{
    return $this->ValorPago;
}

}

?>