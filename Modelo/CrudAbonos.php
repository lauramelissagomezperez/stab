<?php
require_once('../conexion.php');

class CrudAbono{
    public function __construct(){}

    public function IngresarAbono($Abonos){
        $CodigoAbonoAgregado = -1;
        $Db = Db::Conectar();
        //Definir la inserción a realizar.
        $Insert = $Db->prepare('INSERT INTO abonos VALUES(NULL,:Estado,:IdPago,NOW(),:ValorPago)');
        $Insert->bindValue('Estado',$Abonos->getEstado());
        $Insert->bindValue('IdPago',$Abonos->getIdPago());
        $Insert->bindValue('ValorPago',$Abonos->getValorPago());		
        try{
            $Insert->execute();
            $CodigoAbonoAgregado = $Db->LastInsertId();
            echo "<script>swal({
                title: 'Éxito',
                text: 'El pago se ha generado exitosamente',
                type: 'success',
              }, function(confirm){
                if(confirm){
                  window.location.href = 'ListadoPagos.php?pagina=1';
                }
              })
            </script>";
        }
        catch(Exception $e){
            echo $e->getMessage();
            die();
        }
        return $CodigoAbonoAgregado;
    }

    


}

?>