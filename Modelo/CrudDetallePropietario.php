<?php

require_once('../conexion.php');


Class CrudDetallePropietario{
    
    public function __construct(){}

    public function InsertarDetallePropietario($DetallePropietario)
    {
        $CodigoDetallePropietariogenerado = 0;
        $Db = Db::Conectar(); //Conectar a la BD                   
                    $Insert = $Db->prepare('INSERT INTO detallepropietarioapartamento (IdPropietarioApto,NApartamento,CedulaPropietario) VALUES (NULL,:NApartamento,:CedulaPropietario)');
                    $Insert->bindValue('NApartamento',$DetallePropietario->getNApartamento());
                    $Insert->bindValue('CedulaPropietario',$DetallePropietario->getCedulaPropietario());

                    try{
                        $Insert->execute();
                        $CodigoDetallePropietariogenerado = $Db->LastInsertId();
                        echo "";

                    }catch(Exception $e){
                        echo $e->getMessage();
                        die();
                    }
                    return $CodigoDetallePropietariogenerado;
    }

    public function MostrarDetalle($CedulaPropietario)
    {
      $Db = Db::Conectar();
      $Sql = $Db->prepare('SELECT * FROM detallepropietarioapartamento WHERE detallepropietarioapartamento.CedulaPropietario=:CedulaPropietario');
      $Sql->bindValue('CedulaPropietario',$CedulaPropietario);
      $MyDetalle = new DetallePropietario();
      try{

        $Sql->execute();
        $Propietario = $Sql->fetch(); // se almacena en la variable $Pago los datos de la variable SQL
        $MyDetalle->setCedulaPropietario($Propietario['CedulaPropietario']);
        $MyDetalle->setNApartamento($Propietario['NApartamento']);
      }catch(Exception $e){
        echo $e->getMessage();
        die();

      }
      return $MyDetalle;
    }

    public function EditarDetallePropietario($DetallePropietario)
    {
        $edicionexitoso = -1; 
        $Db = Db::Conectar(); //Conectar a la BD
        $Sql = $Db->prepare('UPDATE detallepropietarioapartamento SET NApartamento=:NApartamento WHERE CedulaPropietario=:CedulaPropietario');
        $Sql->bindValue('CedulaPropietario',$DetallePropietario->getCedulaPropietario());
        $Sql->bindValue('NApartamento',$DetallePropietario->getNApartamento());

        try{
            $Sql->execute();
            $edicionexitoso = $Db->LastInsertId();
           
        }catch(Exception $e){
            echo $e->getMessage();
            die();
        }
        return $edicionexitoso;
    }
    
    public function CambiarDeEstado ($Cedula){
      $Db = Db::Conectar();
      $Sql = $Db->prepare('UPDATE propietarios set IdEstado=2 WHERE Cedula=:Cedula');
      $Sql->bindValue('Cedula',$Cedula);
     
      try{
          $Sql->execute();

          echo "<script>swal({
            title: 'Éxito',
            text: 'El estado del propietario se actualizó correctamente',
            type: 'success',
          }, function(confirm){
            if(confirm){
              window.location.href ='../Vistas/ListadoPropietarios.php';
            }
          })
        </script>";

      }
      catch(Exeption $e){
          echo $e->getMessage();
          die();
      }
  }


  public function CambiarDeEstadoActivo ($Cedula){
    $Db = Db::Conectar();
    $Sql = $Db->prepare('UPDATE propietarios set IdEstado=1 WHERE Cedula=:Cedula');
    $Sql->bindValue('Cedula',$Cedula);
   
    try{
        $Sql->execute();

        echo "<script>swal({
          title: 'Éxito',
          text: 'El estado del propietario se actualizó correctamente',
          type: 'success',
        }, function(confirm){
          if(confirm){
            window.location.href ='../Vistas/ListadoPropietarios.php';
          }
        })
      </script>";

    }
    catch(Exeption $e){
        echo $e->getMessage();
        die();
    }
}

               
}

?>