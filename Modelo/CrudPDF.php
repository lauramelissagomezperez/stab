<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>

<?php
   
    class CrudPDF
    {

            public function __construct(){
            }
               

            public function PDFPago($NCuentaCobro)
            {
              $Db = Db::Conectar();
              $Sql = $Db->query('SET lc_time_names = "es_ES";');
              $Sql = $Db->prepare('SELECT *,da.CedulaPropietario,DATE_FORMAT(Periodo, "%M %Y") AS Periodo,
              DATE_FORMAT(PeriodoFin, "%M %Y") AS PeriodoFin,
              DATE_FORMAT(Fecha, "%d-%m-%Y") AS Fecha,
              DATE_FORMAT(FechaLimite, "%d-%m-%Y %r") AS FechaLimite 
              FROM pagos INNER JOIN detallepropietarioapartamento da ON (da.NApartamento=pagos.NApartamento) WHERE NCuentaCobro=:NCuentaCobro');
              $Sql->bindValue('NCuentaCobro',$NCuentaCobro);
              $MyPago = new Pago();
              try{

                $Sql->execute();
                $Pago = $Sql->fetch(); // se almacena en la variable $Pago los datos de la variable SQL
                $MyPago->setNCuentaCobro($Pago['NCuentaCobro']);
                $MyPago->setPeriodo($Pago['Periodo']);
                $MyPago->setPeriodoFin($Pago['PeriodoFin']);
                $MyPago->setFecha($Pago['Fecha']);
                $MyPago->setFechaLimite($Pago['FechaLimite']);
                $MyPago->setNApartamento($Pago['NApartamento']);
                $MyPago->setPropietario($Pago['CedulaPropietario']);
                $MyPago->setDireccionEntrega($Pago['DireccionEntrega']);
                $MyPago->setCorreo($Pago['Correo']);
                $MyPago->setCuartoUtil($Pago['ValorCuartoUtil']);
                $MyPago->setM2($Pago['ValorMetrosCuadrados']);
                $MyPago->setTotalPagar($Pago['TotalPagar']);
                $MyPago->setValorParqueadero($Pago['ValorParqueadero']);
                $MyPago->setObservaciones($Pago['Observaciones']);
                
              }catch(Exception $e){
                echo $e->getMessage();
                die();

              }
              return $MyPago;
            }
    }

?>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>