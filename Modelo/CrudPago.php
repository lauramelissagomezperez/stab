<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>

<?php

require_once('../conexion.php');
   
    class CrudPago
    {

            public function __construct(){
            }
                public function InsertarPago($Pago)
                {
                    $Db = Db::Conectar(); //Conectar a la BD                   
                    $Insert = $Db->prepare('INSERT INTO pagos VALUES(:NCuentaCobro,:Correo,:DireccionEntrega,:Fecha,:FechaLimite,:IdEstado,:NApartamento,:Observaciones,:Periodo,:PeriodoFin,:Propietario,:TipoPago,:TotalPagar,:ValorCuartoUtil,:ValorMetrosCuadrados,:ValorMulta,:SaldoaFavor,:ValorParqueadero,:Residente)');
                    $Insert->bindValue('NCuentaCobro',$Pago->getNCuentaCobro());
                    $Insert->bindValue('Correo',$Pago->getCorreo());
                    $Insert->bindValue('DireccionEntrega',$Pago->getDireccionEntrega());
                    $Insert->bindValue('Fecha',$Pago->getFecha());
                    $Insert->bindValue('FechaLimite',$Pago->getFechaLimite());
                    $Insert->bindValue('IdEstado',$Pago->getIdEstado());
                    $Insert->bindValue('NApartamento',$Pago->getNApartamento());
                    $Insert->bindValue('Observaciones',$Pago->getObservaciones());
                    $Insert->bindValue('Periodo',$Pago->getPeriodo());
                    $Insert->bindValue('PeriodoFin',$Pago->getPeriodoFin());
                    $Insert->bindValue('Propietario',$Pago->getPropietario());
                    $Insert->bindValue('TipoPago',$Pago->getTipoPago());
                    $Insert->bindValue('TotalPagar',$Pago->getTotalPagar());
                    $Insert->bindValue('ValorCuartoUtil',$Pago->getCuartoUtil());
                    $Insert->bindValue('ValorMetrosCuadrados',$Pago->getM2());
                    $Insert->bindValue('ValorMulta',$Pago->getValorMulta());
                    $Insert->bindValue('SaldoaFavor',$Pago->getSaldoaFavor());
                    $Insert->bindValue('ValorParqueadero',$Pago->getValorParqueadero());
                    $Insert->bindValue('Residente',$Pago->getResidente());

                    
                    
                    
                    try{
                        $Insert->execute();
                        echo "<script>swal({
                            title: 'Éxito',
                            text: 'El pago se ha generado exitosamente',
                            type: 'success',
                          }, function(confirm){
                            if(confirm){
                              window.location.href = 'ListadoPagos.php?pagina=1';
                            }
                          })
                        </script>";

                    }catch(Exception $e){
                        echo "<script>swal({
                          title: '¡Oh oh, no hemos podido generar el pago!',
                          text: 'Hubo algun error, pero no sabemos que D:',
                          type: 'error',
                        }, function(confirm){
                          if(confirm){
                            window.location.href = 'ListadoPagos.php?pagina=1';
                          }
                        })
                      </script>";
                        die();
                    }
              }

            public function ListarPago()
            {
                $Db = Db::Conectar();
                $ListaPago = [];
                $Sql = $Db->query('SET lc_time_names = "es_ES";');
                $Sql = $Db->query('SELECT *,DATE_FORMAT(Periodo, "%M %Y") AS Periodo,estados.NombreEstado, tipospagos.NombreTipoPago as NTotalPagar, pagos.TotalPagar-SUM(abonos.ValorPago) as NTotalPagar from pagos inner join tipospagos on (pagos.TipoPago = tipospagos.IdTiposPagos) inner JOIN estados on (pagos.IdEstado = estados.IdEstado) LEFT JOIN abonos on (pagos.NCuentaCobro=abonos.IdPago) WHERE abonos.IdPago GROUP BY pagos.NCuentaCobro
                UNION
                SELECT *,DATE_FORMAT(Periodo, "%M %Y") AS Periodo,estados.NombreEstado, tipospagos.NombreTipoPago as NTotalPagar, pagos.TotalPagar-SUM(abonos.ValorPago) as NTotalPagar from pagos inner join tipospagos on (pagos.TipoPago = tipospagos.IdTiposPagos) inner JOIN estados on (pagos.IdEstado = estados.IdEstado) LEFT JOIN abonos on (pagos.NCuentaCobro=abonos.IdPago) WHERE abonos.IdPago IS NULL GROUP BY pagos.NCuentaCobro ORDER BY NCuentaCobro DESC');

                $Sql->execute();

                //Paginación

                $Total_filas = $Sql->rowCount();
                foreach ($Sql->fetchAll() as $Pago)
                {
                    $MyPago = new Pago();
                    $MyPago->setNCuentaCobro($Pago['NCuentaCobro']);
                    $MyPago->setTipoPago($Pago['NombreTipoPago']);
                    $MyPago->setTotalPagar($Pago['TotalPagar']);
                    $MyPago->setNapartamento($Pago['NApartamento']);
                    $MyPago->setPeriodo($Pago['Periodo']);
                    $MyPago->setIdEstado($Pago['NombreEstado']);
                    $MyPago->setNTotalPagar($Pago['NTotalPagar']);

                    
                    $ListaPago[] = $MyPago;
                }
                return array($ListaPago, $Total_filas);
            }



            public function ActualizarPago($Pago){
              $CodigoActualizarPago = 0;
                $Db = Db::Conectar();
                $Sql = $Db->prepare('UPDATE pagos SET IdEstado=:IdEstado WHERE NCuentaCobro=:NCuentaCobro');
                $Sql->bindValue('NCuentaCobro',$Pago->getNCuentaCobro());
                $Sql->bindValue('IdEstado',$Pago->getIdEstado());

                try{
                    $Sql->execute();
                    $CodigoActualizarPago = $Db->LastInsertId();
                    echo "<script>swal({
                        title: 'Éxito',
                        text: 'El pago se ha generado exitosamente',
                        type: 'success',
                      }, function(confirm){
                        if(confirm){
                          window.location.href = 'ListadoPagos.php?pagina=1';
                        }
                      })
                    </script>";

                }
                catch(Exeption $e){
                    echo $e->getMessage();
                    die();
                }
                return $CodigoActualizarPago;
            }

            public function ObtenerPago($NCuentaCobro){
              $Db = Db::Conectar();
              $Sql = $Db->prepare('SELECT p.NCuentaCobro,p.ValorParqueadero,p.Observaciones, p.TotalPagar, p.ValorCuartoUtil,p.SaldoaFavor, p.ValorMulta, p.ValorMetrosCuadrados, p.Fecha, p.Periodo, p.FechaLimite, p.NApartamento, pr.Nombre, p.DireccionEntrega, pr.Correo from pagos p
              INNER JOIN detallepropietarioapartamento dpa ON (p.NApartamento=dpa.NApartamento)
              INNER JOIN propietarios pr ON (dpa.CedulaPropietario=pr.Cedula)
              WHERE NCuentaCobro=:NCuentaCobro');
              $Sql->bindValue('NCuentaCobro', $NCuentaCobro);
              $MyPago = new Pago();
              try{
                  $Sql->execute();
                  $pago = $Sql->fetch();
                  $MyPago->setNCuentaCobro($pago['NCuentaCobro']);                
                  $MyPago->setFecha($pago['Fecha']);
                  $MyPago->setPeriodo($pago['Periodo']);
                  $MyPago->setFechaLimite($pago['FechaLimite']);
                  $MyPago->setNApartamento($pago['NApartamento']);
                  $MyPago->setPropietario($pago['Nombre']);
                  $MyPago->setDireccionEntrega($pago['DireccionEntrega']);
                  $MyPago->setCorreo($pago['Correo']);
                  $MyPago->setValorParqueadero($pago['ValorParqueadero']);
                  $MyPago->setCuartoUtil($pago['ValorCuartoUtil']);
                  $MyPago->setM2($pago['ValorMetrosCuadrados']);
                  $MyPago->setValorMulta($pago['ValorMulta']);
                  $MyPago->setSaldoaFavor($pago['SaldoaFavor']);
                  $MyPago->setTotalPagar($pago['TotalPagar']);
                  $MyPago->setObservaciones($pago['Observaciones']);
              }
              catch(Exception $e){
                  echo $e->getMessage();
                  die();
              }
              return $MyPago;
          }
            
    }

?>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>