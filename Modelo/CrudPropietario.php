<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>

<?php

require_once('../conexion.php');


Class CrudPropietario{
    
    public function __construct(){}

    public function InsertarPropietario($Propietario)
    {
        $CodigoPropietariogenerado = -1;
        $Db = Db::Conectar(); //Conectar a la BD                   
                    $Insert = $Db->prepare('INSERT INTO propietarios VALUES(:Cedula,:Nombre,:Correo,:Direccion,:IdEstado,:RecibirFisico,:RecibirCorreo,:Telefono,:Celular)');
                    $Insert->bindValue('Cedula',$Propietario->getCedula());
                    $Insert->bindValue('Nombre',$Propietario->getNombre());
                    $Insert->bindValue('Telefono',$Propietario->getTelefono());
                    $Insert->bindValue('Direccion',$Propietario->getDireccion());
                    $Insert->bindValue('Correo',$Propietario->getCorreo());
                    $Insert->bindValue('IdEstado',$Propietario->getIdEstado());
                    $Insert->bindValue('RecibirCorreo',$Propietario->getRecibirCorreo());
                    $Insert->bindValue('RecibirFisico',$Propietario->getRecibirFisico());
                    $Insert->bindValue('Celular',$Propietario->getCelular());

                    try{
                        $Insert->execute();
                        $CodigoPropietariogenerado = $Db->LastInsertId();
                        
                    }catch(Exception $e){
                      
                        die();
                    }
                    return $CodigoPropietariogenerado;
    }

    public function MostrarPropietario($Cedula)
    {
      $Db = Db::Conectar();
      $Sql = $Db->prepare('SELECT p.Cedula,p.Nombre,p.Correo,p.Direccion,p.Telefono,p.RecibirCorreo,p.RecibirFisico,p.Celular,e.NombreEstado FROM propietarios p INNER JOIN estados e ON (p.IdEstado=e.IdEstado) WHERE Cedula=:Cedula');
      $Sql->bindValue('Cedula',$Cedula);
      $MyPropietario = new Propietario();
      try{

        $Sql->execute();
        $Propietario = $Sql->fetch(); // se almacena en la variable $Pago los datos de la variable SQL
        $MyPropietario->setCedula($Propietario['Cedula']);
        $MyPropietario->setNombre($Propietario['Nombre']);
        $MyPropietario->setTelefono($Propietario['Telefono']);
        $MyPropietario->setDireccion($Propietario['Direccion']);
        $MyPropietario->setCorreo($Propietario['Correo']);
        $MyPropietario->setIdEstado($Propietario['NombreEstado']);
        $MyPropietario->setRecibirCorreo($Propietario['RecibirCorreo']);
        $MyPropietario->setRecibirFisico($Propietario['RecibirFisico']);
        $MyPropietario->setCelular($Propietario['Celular']);
      }catch(Exception $e){
        echo $e->getMessage();
        die();

      }
      return $MyPropietario;
    }

    public function MostrarEstado($Cedula)
    {
      $Db = Db::Conectar();
      $Sql = $Db->prepare('SELECT * FROM estados e join propietarios p on e.IdEstado=:Cedula');
      $Sql->bindValue('Cedula',$Cedula);
      $MyPropietario = new Estado();
      try{

        $Sql->execute();
        $Propietario = $Sql->fetch(); // se almacena en la variable $Pago los datos de la variable SQL
        $MyPropietario->setCedula($Propietario['Cedula']);
      }catch(Exception $e){
        echo $e->getMessage();
        die();

      }
      return $MyPropietario;
    }

    public function EditarPropietario($Propietario)
                {
                    $CodigoPropietarioeditado = -1;
                    $Db = Db::Conectar(); //Conectar a la BD
                    $Sql = $Db->prepare('UPDATE propietarios SET Nombre=:Nombre,Telefono=:Telefono,Direccion=:Direccion,Correo=:Correo,
                    RecibirCorreo=:RecibirCorreo,RecibirFisico=:RecibirFisico,Celular=:Celular
                    WHERE Cedula=:Cedula');
                    $Sql->bindValue('Cedula',$Propietario->getCedula());
                    $Sql->bindValue('Nombre',$Propietario->getNombre());
                    $Sql->bindValue('Telefono',$Propietario->getTelefono());
                    $Sql->bindValue('Direccion',$Propietario->getDireccion());
                    $Sql->bindValue('Correo',$Propietario->getCorreo());
                   
                    $Sql->bindValue('RecibirCorreo',$Propietario->getRecibirCorreo());
                    $Sql->bindValue('RecibirFisico',$Propietario->getRecibirFisico());
                    $Sql->bindValue('Celular',$Propietario->getCelular());

                    try{
                        $Sql->execute();
                        $CodigoPropietarioeditado = $Db->LastInsertId();
                        echo "<script>swal({
                            title: 'Éxito',
                            text: 'El propietario se ha actualizado exitosamente',
                            type: 'success',
                          }, function(confirm){
                            if(confirm){
                              window.location.href = '../Vistas/ListadoPropietarios.php'
                            }
                          })
                        </script>";

                    }catch(Exception $e){
                        echo $e->getMessage();
                        die();
                    }
                    return $CodigoPropietarioeditado;
                }

                
    
}

?>


</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>