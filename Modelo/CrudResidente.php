<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>

<?php

require_once('../conexion.php');


Class CrudResidente{
    
    public function __construct(){}

    public function InsertarResidente($Residente)
    {
        $CodigoResidentegenerado = -1;
        $Db = Db::Conectar(); //Conectar a la BD                   
                    $Insert = $Db->prepare('INSERT INTO residentes VALUES(:Cedula,:Nombre,:IdEstado,:NApartamento,:Correo,:Observaciones,:Telefono,:Celular)');
                    $Insert->bindValue('Cedula',$Residente->getCedula());
                    $Insert->bindValue('Nombre',$Residente->getNombre());
                    $Insert->bindValue('IdEstado',$Residente->getIdEstado());
                    $Insert->bindValue('NApartamento',$Residente->getNApartamento());
                    $Insert->bindValue('Correo',$Residente->getCorreo());
                    $Insert->bindValue('Observaciones',$Residente->getObservaciones()); 
                    $Insert->bindValue('Telefono',$Residente->getTelefono());
                    $Insert->bindValue('Celular',$Residente->getCelular());
                   
        

                    try{
                        $Insert->execute();
                        $CodigoResidentegenerado = $Db->LastInsertId();

                    }catch(Exception $e){
                      
                        die();
                    }
                    return $CodigoResidentegenerado;
    }

    public function MostrarResidente($Cedula)
    {
      $Db = Db::Conectar();
      $Sql = $Db->prepare('SELECT r.Cedula,r.Nombre,r.NApartamento,r.Correo,r.Observaciones,r.Telefono,r.Celular,e.NombreEstado FROM residentes r INNER JOIN estados e ON (r.IdEstado=e.IdEstado) WHERE Cedula=:Cedula');
      $Sql->bindValue('Cedula',$Cedula);
      $MyResidente = new Residente();
      try{

        $Sql->execute();
        $Residente = $Sql->fetch(); // se almacena en la variable $Pago los datos de la variable SQL
        $MyResidente->setCedula($Residente['Cedula']);
        $MyResidente->setNombre($Residente['Nombre']);
        $MyResidente->setIdEstado($Residente['NombreEstado']);
        $MyResidente->setNApartamento($Residente['NApartamento']);
        $MyResidente->setCorreo($Residente['Correo']);
        $MyResidente->setObservaciones($Residente['Observaciones']);
        $MyResidente->setTelefono($Residente['Telefono']);
        $MyResidente->setCelular($Residente['Celular']);
      
      }catch(Exception $e){
        echo $e->getMessage();
        die();

      }
      return $MyResidente;
    }

    public function EditarResidente($Residente)
                {
                    $CodigoResidenteeditado = -1;
                    $Db = Db::Conectar(); //Conectar a la BD
                    $Sql = $Db->prepare('UPDATE Residentes SET Nombre=:Nombre,NApartamento=:NApartamento,Correo=:Correo,Observaciones=:Observaciones,
                    Telefono=:Telefono, Celular=:Celular
                    WHERE Cedula=:Cedula');
                    $Sql->bindValue('Cedula',$Residente->getCedula());
                    $Sql->bindValue('Nombre',$Residente->getNombre());
                    $Sql->bindValue('NApartamento',$Residente->getNApartamento());
                    $Sql->bindValue('Correo',$Residente->getCorreo());
                    $Sql->bindValue('Observaciones',$Residente->getObservaciones());
                    $Sql->bindValue('Telefono',$Residente->getTelefono());
                    $Sql->bindValue('Celular',$Residente->getCelular());

                    try{
                        $Sql->execute();
                        $CodigoResidenteeditado = $Db->LastInsertId();
                        echo "<script>swal({
                            title: 'Éxito',
                            text: 'El Residente se ha actualizado exitosamente',
                            type: 'success',
                          }, function(confirm){
                            if(confirm){
                              window.location.href = '../Vistas/ListadoResidentes.php';
                            }
                          })
                        </script>";

                    }catch(Exception $e){
                        echo $e->getMessage();
                        die();
                    }
                    return $CodigoResidenteeditado;
                }


                public function CambiarDeEstado ($Cedula){
                  $Db = Db::Conectar();
                  $Sql = $Db->prepare('UPDATE residentes set IdEstado=2 WHERE Cedula=:Cedula');
                  $Sql->bindValue('Cedula',$Cedula);
                 
                  try{
                      $Sql->execute();
            
                      echo "<script>swal({
                          title: 'Éxito',
                          text: 'El estado del residente se actualizó correctamente',
                          type: 'success',
                        }, function(confirm){
                          if(confirm){
                            window.location.href ='../Vistas/ListadoResidentes.php';
                          }
                        })
                      </script>";
            
                  }
                  catch(Exeption $e){
                      echo $e->getMessage();
                      die();
                  }
              }

              public function CambiarDeEstadoActivo ($Cedula){
                $Db = Db::Conectar();
                $Sql = $Db->prepare('UPDATE residentes set IdEstado=1 WHERE Cedula=:Cedula');
                $Sql->bindValue('Cedula',$Cedula);
               
                try{
                    $Sql->execute();
          
                    echo "<script>swal({
                        title: 'Éxito',
                        text: 'El estado del residente se actualizó correctamente',
                        type: 'success',
                      }, function(confirm){
                        if(confirm){
                          window.location.href ='../Vistas/ListadoResidentes.php';
                        }
                      })
                    </script>";
          
                }
                catch(Exeption $e){
                    echo $e->getMessage();
                    die();
                }
            }
            
                
    
}

?>


</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>