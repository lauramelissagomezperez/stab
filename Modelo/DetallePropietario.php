<?php

    class DetallePropietario{
        //Parametros de entrada
        private $IdPropietarioApto;
        private $NApartamento;
        private $CedulaPropietario;

        //Definir constructor
        public function __construct(){}
        
        //Definir los métodos set y get

        public function setIdPropietarioApto($IdPropietarioApto)
        {
            $this->IdPropietarioApto = $IdPropietarioApto;
        }
        public function getIdPropietarioApto()
        {
            return $this->IdPropietarioApto;
        }

        public function setNApartamento($NApartamento)
        {
            $this->NApartamento = $NApartamento;
        }
        public function getNApartamento()
        {
            return $this->NApartamento;
        }

        public function setCedulaPropietario($CedulaPropietario)
        {
            $this->CedulaPropietario = $CedulaPropietario;
        }
        public function getCedulaPropietario()
        {
            return $this->CedulaPropietario;
        }       

    }


    

?>