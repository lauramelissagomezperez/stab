<?php

    class Propietario{
        //Parametros de entrada
        private $Cedula;
        private $Nombre;
        private $Telefono;
        private $Direccion;
        private $Correo;
        private $IdEstado;
        private $RecibirCorreo;
        private $RecibirFisico;
        private $Celular;

        //Definir constructor
        public function __construct(){}
        
        //Definir los métodos set y get

        public function setCedula($Cedula)
        {
            $this->Cedula = $Cedula;
        }
        public function getCedula()
        {
            return $this->Cedula;
        }

        public function setNombre($Nombre)
        {
            $this->Nombre = $Nombre;
        }
        public function getNombre()
        {
            return $this->Nombre;
        }

        public function setTelefono($Telefono)
        {
            $this->Telefono = $Telefono;
        }
        public function getTelefono()
        {
            return $this->Telefono;
        }

        public function setDireccion($Direccion)
        {
            $this->Direccion = $Direccion;
        }
        public function getDireccion()
        {
            return $this->Direccion;
        }

        public function setCorreo($Correo)
        {
            $this->Correo = $Correo;
        }
        public function getCorreo()
        {
            return $this->Correo;
        }

        public function setIdEstado($IdEstado)
        {
            $this->IdEstado = $IdEstado;
        }
        public function getIdEstado()
        {
            return $this->IdEstado;
        }

        public function setRecibirCorreo($RecibirCorreo)
        {
            $this->RecibirCorreo = $RecibirCorreo;
        }
        public function getRecibirCorreo()
        {
            return $this->RecibirCorreo;
        }

        public function setRecibirFisico($RecibirFisico)
        {
            $this->RecibirFisico = $RecibirFisico;
        }
        public function getRecibirFisico()
        {
            return $this->RecibirFisico;
        }

        public function setCelular($Celular)
        {
            $this->Celular = $Celular;
        }
        public function getCelular()
        {
            return $this->Celular;
        }

    }
    

?>