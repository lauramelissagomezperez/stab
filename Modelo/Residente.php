<?php

    class Residente{
        //Parametros de entrada
        private $Cedula;
        private $Nombre;
        private $IdEstado;
        private $NApartamento;
        private $Correo;
        private $Observaciones;
        private $Telefono;
        private $Celular;
        //Definir constructor
        public function __construct(){}
        
        //Definir los métodos set y get

        public function setCedula($Cedula)
        {
            $this->Cedula = $Cedula;
        }
        public function getCedula()
        {
            return $this->Cedula;
        }

        public function setNombre($Nombre)
        {
            $this->Nombre = $Nombre;
        }
        public function getNombre()
        {
            return $this->Nombre;
        }

        public function setIdEstado($IdEstado)
        {
            $this->IdEstado = $IdEstado;
        }
        public function getIdEstado()
        {
            return $this->IdEstado;
        }

        public function setNApartamento($NApartamento)
        {
            $this->NApartamento = $NApartamento;
        }
        public function getNApartamento()
        {
            return $this->NApartamento;
        }


        public function setCorreo($Correo)
        {
            $this->Correo = $Correo;
        }
        public function getCorreo()
        {
            return $this->Correo;
        }

        public function setObservaciones($Observaciones)
        {
            $this->Observaciones = $Observaciones;
        }
        public function getObservaciones()
        {
            return $this->Observaciones;
        }



        public function setTelefono($Telefono)
        {
            $this->Telefono = $Telefono;
        }
        public function getTelefono()
        {
            return $this->Telefono;
        }

        public function setCelular($Celular)
        {
            $this->Celular = $Celular;
        }
        public function getCelular()
        {
            return $this->Celular;
        }
    

    }
    

?>