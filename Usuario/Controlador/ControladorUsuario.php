<?php
require_once("../../conexion.php");
require_once("../Modelo/Usuario.php");
require_once("../Modelo/CrudUsuario.php");

$Usuario = new Usuario();
$CrudUsuario = new CrudUsuario();

if(isset($_POST["Acceder"])){
    $Usuario->setUsuario($_POST["usuario"]);
    $Usuario->setContrasena(md5($_POST["contrasena"]));

    $Usuario = $CrudUsuario->ValidarAcceso($Usuario);
    if($Usuario->getExiste()==1){
        session_start();
        $_SESSION["usuario"] = $Usuario->getUsuario();
        
        header("Location:../../Vistas/Inicio.php");
    }else{
        ?>
        <script>
        swal({
                            title: 'Error',
                            text: 'El usuario y/o contraseña ingresados son incorrectos',
                            type: 'error',
                          }, function(confirm){
                            if(confirm){
                                document.location.href="../../index.php";
                            }
                          })
        </script>
        <?php
        
    }
}else{
    header("Location:../../index.php");
}

?>