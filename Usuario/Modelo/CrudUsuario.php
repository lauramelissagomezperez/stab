<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/styles.css" />
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>
<?php

class CrudUsuario{
   
    public function __construct(){}

    public function ValidarAcceso($Usuario){
        $Db = Db::Conectar();
        $Sql = $Db->prepare('SELECT * FROM usuarios WHERE Usuario=:Usuario AND Contrasena=:Contrasena');
        $Sql->bindValue('Usuario',$Usuario->getUsuario());
        $Sql->bindValue('Contrasena',$Usuario->getContrasena());
        $Sql->execute(); //Ejecutar la consulta
        $MiUsuario = new Usuario();
        $MiUsuario->setExiste(0);

        if($Sql->rowCount() >0){  //Determinar el numero de registros arrojados por la consulta
            $DatosUsuario= $Sql->fetch(); //Para almacenar los datos arrojados por la consulta
            $MiUsuario->setUsuario($DatosUsuario['usuario']);
            $MiUsuario->setExiste(1);

        }
        return $MiUsuario;
    }
}

?>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>