<?php
class Usuario{
    private $Usuario;
    private $Contrasena;
    private $Existe; // para determinar si el usuario existe o no en la bd

    public function __construct(){

    }

    public function setUsuario($Usuario)
    {
        $this->Usuario= $Usuario;
    }

    public function getUsuario()
    {
        return $this->Usuario;
    }

    public function setContrasena($Contrasena)
    {
        $this->Contrasena= $Contrasena;
    }

    public function getContrasena()
    {
        return $this->Contrasena;
    }

    public function setExiste($Existe)
    {
        $this->Existe= $Existe;
    }

    public function getExiste()
    {
        return $this->Existe;
    }

}
?>