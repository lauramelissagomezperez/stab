<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
require_once('../conexion.php');
require_once('../Modelo/Pago.php');
require_once('../Modelo/CrudPago.php');

$NCuentaCobro = $_GET["NCuentaCobro"];

$MyPago = new CrudPago();
$pago = $MyPago::ObtenerPago($_GET["NCuentaCobro"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>  
    <title>Informes</title>
</head>
<body>
<div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
</div>
<center>
    <h1>DETALLE INFORME</h1>
    <br><br>
    <!--DETALLE PAGOS-->
    <div class="form-horizontal" style="align-content: center">
    <div class="form-group">
        <label for="radio" class="control-label col-xs-3">Entidad:</label> 
        <div class="col-xs-2">
        <input type="text" class="form-control" id="Entidad" name="Entidad" value="Av.Villas" disabled > 
        </div>
        <label for="radio" class="control-label col-xs-3">Tipo de cuenta:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="Corriente" class="form-control" id="TipoCuenta" name="TipoCuenta">
        </div>
    </div>
    <div class="form-group">
        <label for="" class="control-label col-xs-3">Titular:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="C.R. Altobelo P.H" class="form-control" name="Titular" id="Titular" >
        </div>
        <label for="" class="control-label col-xs-3" id="label1">N° cuenta:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="50317248" class="form-control" name="Ncuenta" id="Ncuenta" >
    </div>
    </div> 
    <div class="form-group">
            <label for="" id="label2" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarNcuentaCobro"></label> N° cuenta de cobro:</label> 
            <div class="col-xs-2">
            <input type="text" disabled class="form-control" name="NcuentaCobro" id="NcuentaCobro" value="<?php echo $pago->getNCuentaCobro(); ?>">
            </div>
        
            <label class="control-label col-xs-3" for="" id="label3"><label style="color: red;" for="" id="ValidarPeriodoInicio"></label> Periodo:</label> 
            <div class="col-xs-2">
            <input type="datetime" disabled class="form-control" name="Periodo"  id="Periodo" value="<?php echo $pago->getPeriodo(); ?>">
            </div>
    </div>
        <div class="form-group">
            <label for="" class="control-label col-xs-3">Fecha:</label>
            <div class="col-xs-2">
            <input type="date" disabled name="FechaActual" id="FechaActual" class="form-control date" >
            </div>  
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarFechaLimite"></label> Fecha límite:</label>
            <div class="col-xs-2">
            <input type="datetime" disabled name="FechaLimite" id="FechaLimite" class="form-control" value="<?php echo $pago->getFechaLimite(); ?>">
        </div>
        </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3"><label for=""  style="color: red;"id="ValidarNapartamento"></label> N° apartamento:</label>
        <div class="col-xs-2">
        <input type="text" disabled name="NApartamento" id="NApartamento"  class="form-control" value="<?php echo $pago->getNApartamento(); ?>">
    </div>
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarPropietario"></label>Propietario:</label>
            <div class="col-xs-2">
            <input type="text" disabled name="Propietario"  id="Propietario" class="form-control" value="<?php echo $pago->getPropietario(); ?>">
            </div>  
            <input type="hidden" name="TipoPago" id="TipoPago" class="form-control" value=1>
            <input type="hidden" name="IdEstado" id="IdEstado" class="form-control" value=3>
            
            </div>
            <div class="form-group">
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarDireccionEntrega"></label>Dirección de entrega:</label>
            <div class="col-xs-2">
            <input type="text" disabled name="DireccionEntrega" id="DireccionEntrega" class="form-control" value="<?php echo $pago->getDireccionEntrega(); ?>">
            </div>
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarEmail"></label>Email:</label>
            <div class="col-xs-2">
            <input type="text" disabled id="Email" name="Email" class="form-control" value="<?php echo $pago->getCorreo(); ?>">
            </div>
            </div>
            <br><br>

            <label for="" for="" id="labelDetalles">Conceptos que intervienen en la cuota de administración</label>
                    <br>
                    <br>
                    <label for="" class="control-label col-xs-3">Concepto</label> 
                    <label for="" class="control-label col-xs-3">Valor</label> 
                    <label for="" class="control-label col-xs-3">Observaciones</label>
                    <br><br>
                    

                    <div class="content">
                    <div class="left">
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarParqueadero"></label>Parqueadero</p> 
                    <div class="midiv col-xs-1">
                    <input class="form-control" name="Parqueadero" id="Parqueadero" readonly value="<?php echo $pago->getValorParqueadero(); ?>"  style="text-align:right">
                    </div>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarCuartoutil"></label>Cuarto útil</p> 
                    <div class="midiv col-xs-1">
                    <input for="" class="form-control" name="CuartoUtil" id="CuartoUtil" readonly value="<?php echo $pago->getCuartoUtil(); ?>" style="text-align:right"> 
                    </div>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarM2"></label>Costo base</p> 
                    <div class="midiv col-xs-1">
                    <input class="form-control" name="M2" readonly  value="<?php echo $pago->getM2(); ?>" id="M2" style="text-align:right"> 
                    </div>
                    
                    <div>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="Validarmulta"></label>Multa</p> 
                    <div class="midiv col-xs-1">
                    <input class="form-control" readonly name="ValorMulta"  value="<?php echo $pago->getValorMulta(); ?>"  id="ValorMulta" style="text-align:right"> 
                    </div></div>

                    <div>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="Validarsaldo"></label>Saldo a favor</p> 
                    <div class="midiv col-xs-1">
                    <input class="form-control" name="SaldoaFavor"  readonly value="<?php echo $pago->getSaldoaFavor(); ?>"  id="SaldoaFavor" style="text-align:right"> 
                    </div></div>
                    
                    <br><br>
                    <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarPagoTotal"></label> Total a pagar:</label>
                    <div class="midiv col-xs-1">
                    <input type="text" id="TotalPagar" readonly value="<?php echo $pago->getTotalPagar(); ?>" name="TotalPagar" class="form-control" style="text-align:right">
                    </div>
        </div>
        <div class="right">
            <textarea name="Observaciones" readonly class="form-control" id="Observaciones" rows="7"><?php echo $pago->getObservaciones(); ?></textarea>
        </div>
    </div><br><br>
    <br><br>
    <br><br>
    <br><br>
    
        
                    <label for="" id="labelDetalles">Abonos realizados</label>
                    <br>
                    <br>
                    

                    <div class="content">
                    <!--TABLA  DE ABONOS-->
                    <table class="table" style="width:60%">
                        <thead class="thead-striped">
                            <tr>
                                <th>Fecha Abono</th>
                                <th>Estado</th>
                                <th>Valor Abono</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                                $Db = Db::Conectar();
                                $Sql = $Db->query('SELECT ab.IdAbono,ab.IdPago,ab.FechaPago,ab.ValorPago,e.NombreEstado from abonos ab
                                INNER JOIN estados e ON (ab.Estado=e.IdEstado)
                                INNER JOIN pagos p ON (ab.IdPago="'.$NCuentaCobro.'")
                                WHERE p.NCuentaCobro="'.$NCuentaCobro.'"');
                                $Sql->execute();
                                $TotalAbonos = 0;
                                while($row=$Sql->fetch(PDO::FETCH_ASSOC))
                                {
                                extract((array)$row);
                                $Valor = $row["ValorPago"];
                                ?>
                                <tr>
                                    <td><?php echo $row["FechaPago"];?></td>
                                    <td><?php echo $row["NombreEstado"];?></td>
                                    <td><?php echo '$' . number_format($Valor, 2);?></td>
                                    </tr>
                                <?php
                                $TotalAbonos = $TotalAbonos + $Valor;
                                }
                            ?>
                        </tbody>
                    </table>
                    <!--FIN TABLA DE ABONOS-->
                    <br>
                    <label for="" class="control-label col-xs-5"><label for="" style="color: red;" id="ValidarPagoTotal"></label> Total abonado:</label>
                    <div class="midiv col-xs-10">
                    <input type="text" id="TotalNPagar" readonly name="TotalNPagar" class="form-control" style="text-align:right" value="<?php echo '$' . number_format($TotalAbonos, 2); ?>">
                    </div>
        </div>
    </div>
    <!--FIN DETALLE PAGOS-->
<center>
    <br><br><br>
                <button type="button" onclick="VolverI()" class="btn btn-info">Volver</button>
    </center>
<br><br><br><br>
<footer align="center" class="border-top footer" style="font-family:fantasy">
    <div class="container">
        &copy; 2020 - S.T.A.B
    </div>
</footer>
</body>
<script src="../js/funciones.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>