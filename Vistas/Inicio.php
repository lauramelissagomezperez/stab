<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <title>Lista</title>
</head>
<body style="background: url(../img/FondoInicio.jpg) no-repeat center center fixed; background-size: cover;">
    <div class="Inicio">
  <div class="DivPagos">
      <a href="ListadoPagos.php?pagina=1" style="text-decoration: none;"><h1 class="h1Pagos">PAGOS</h1>
          <img src="../img/Pagos.png" class="ImgPagos" alt=""></a>
        </div>
  <div class="DivArriba">
      <div class="DivApartamentos">
      <a href="ListarApartamento.php?pagina=1" style="text-decoration: none;"><h1 class="h1Apartamentos">APARTAMENTOS</h1>
          <img src="../img/Apartamentos.png" class="ImgApartamentos" alt=""></a>
      </div>
      <div class="DivPropietarios">
      <a href="ListadoPropietarios.php?pagina=1" style="text-decoration: none;">
          <img src="../img/Propietarios.png" class="ImgPropietarios" alt=""><p class="h1Propietarios">PROPIETARIOS</p></a>
      </div>
  </div>
  <div class="DivAbajo">
      <div class="DivInformes">
      <a href="Informes.php?pagina=1" style="text-decoration: none;">
          <img src="../img/Informes.png" class="ImgInformes" alt=""><p class="h1Informes">INFORMES</p></a>
         </div>
      <div class="DivResidentes"> 
      <a href="ListadoResidentes.php?pagina=1" style="text-decoration: none;">
          <img src="../img/Residentes.png" class="ImgResidentes" alt=""><p class="h1Residentes">RESIDENTES</p></a>
      </div>
      <div class="DivCerrarSesion"> 
      <a href="../CerrarSesion.php" style="text-decoration: none;">
          <img src="../img/CerrarSesion.png" class="ImgCerrarSesion" alt=""><p class="h1CerrarSesion">CERRAR SESIÓN</p></a>
      </div>
  </div>
</div>
<footer align="center" class="border-top footer" style="font-family: sans-serif; color:white;font-weight: 700;">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
</body>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
</html>