<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
require_once('../conexion.php');
require_once('../Modelo/Pago.php');
require_once('../Modelo/CrudPago.php');
$CrudPago = new CrudPago();
list($ListaPago, $Total_filas) = $CrudPago->ListarPago();

$filas_por_pagina = 10;

//contar el número de filas

$paginas = $Total_filas/$filas_por_pagina;
$paginas = ceil($paginas);

if ($_GET['pagina']>$paginas) {
  header('Location:ListadoPagos.php?pagina=1');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <title>Pago adelantado</title>
</head>
<body>

<div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
</div>
  <center>
    <h1 style="font-family:fantasy">PAGOS GENERADOS</h1>
    
    <div class="content">
                    <div class="leftboton">
    <a data-toggle="modal" data-target="#dialogo1" title="Agregar pago"><i class="far fa-plus-square fa-3x"></i></a>
    </div>
    <div class="buscar">
<label for="radio" class="control-label col-xs-3">Buscar:</label>
    <div class="col-xs-6">
        <input id="entradafilter" type="text" class="form-control">
    </div>
</div></div>
<!--------MODAL--------------------------------------->
<div class="modal fade" id="dialogo1" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
    
          <!-- cabecera del diálogo -->
          <div class="modal-header">
            <h4 class="modal-title">TIPO DE PAGO A GENERAR</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times-circle"></i></button>
          </div>
    
          <!-- cuerpo del diálogo -->
          
          <div class="modal-body">
          <div class="btn-group" role="group" aria-label="Basic example">
          <button type="button" class="btn btn-secondary" onclick="window.location='CuotaAdministracion.php'">Cuota de administración</button>
          <button type="button" class="btn btn-secondary" onclick="window.location='PagoAdelantado.php'">Pago adelantado</button>
          <button type="button" class="btn btn-secondary" onclick="window.location='Multa.php'">Multa</button>
          </div>
          </div>
        </div>
      </div>
    </div> 

    <!--------MODAL  AGREGAR ABONO--------------------------------------->
<div class="modal fade" id="dialogo2" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
    
          <!-- cabecera del diálogo -->
          <div class="modal-header">
            <h4 class="modal-title">ABONO O PAGO COMPLETO</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times-circle"></i></button>
          </div>
    
          <!-- cuerpo del diálogo -->
          <div class="modal-body">
            <input onchange="showContent()" type="radio"id="Abono" name="numero" value="Abono"> Abono
            <input onchange="showContent()"type="radio"id="PagoCompleto" name="numero" value="PagoCompleto"> Pago completo
        <br><br>
        <form class="form-horizontal" id="frmAbono" name="frmAbono" action="../Controlador/ControladorPago.php" style="align-content: center" method="POST">
        <div id="MostrarAbono" style="display: none; position:relative; left:80px;">
        <div class="form-group">
        <label for="" class="control-label col-xs-4"><label style="color: red;" for="" id="ValidarAbono"></label>Valor deuda:</label> 
        <div class="col-xs-3 innermodal">
         <input type="text" readonly name="pagocompleto" id="pagocompleto">
         </div>
          </div>
          <div class="form-group">
          <label for="inputlg" class="control-label col-xs-4"><label style="color: red;" for="" id="ValidarAbono"></label>Valor a abonar:</label> 
        <div class="col-xs-3">
            
            <input type="text" class="form-control abonos" name="abono" id="abono" onmousemove="CambiodeEstado()">
            <input type="hidden" readonly class="form-control" name="ncuentacobro" id="ncuentacobro">
            <input type="hidden" readonly class="form-control" name="estado" id="estado" value="">
            <br>
            <input type="hidden" name="AgregarAbono" id="AgregarAbono">
            <button type="submit" class="btn btn-success">Registrar</button>
            </div>
            
            </div>
          </div>
           
            </form>
            <p align="center" id="RespuestaTransaccionAbono"></p>
            <form class="form-horizontal" action="../Controlador/ControladorPago.php" style="align-content: center" method="POST">
            <div id="MostrarPagoCompleto" style="display: none; position:relative; left:100px;">
            <label for="" class="control-label col-xs-2"><label style="color: red;" for="" id="ValidarNcuentaCobro"></label>Total:</label> 
            <div class="col-xs-3">
            <input type="text" readonly class="form-control" name="pagocompleto" id="pagocompleto">
            <input type="hidden" readonly class="form-control" name="ncuentacobro" id="ncuentacobro">
            <input type="hidden" readonly class="form-control" name="estado" id="estado" value="5">
            <br>
            <input type="hidden" name="AgregarPagoCompleto" id="AgregarPagoCompleto">
            <button type="submit" class="btn btn-info" onclick="Validarpagomayor()">Registrar</button>
            </form>
            </div>
            </div>
    </div>
    <br>
    
        </div>
      </div>
    </div> 

<table class="table table-hover"  style="width:80%" id="tblPagos">
  <thead class="">
    <tr>
      <th scope="col">N° cuenta cobro</th>
      <th scope="col">Pendiente a pagar</th>
      <th scope="col">Tipo de pago</th>
      <th scope="col">Número de apartamento</th>
      <th scope="col">Periodo</th>
      <th scope="col">Estado</th>
      <th scope="col">Opciones</th>
    </tr>
  </thead>
  <tbody class="contenidobusqueda">
  <?php
   //PAGINACIÓN
   $Db = Db::Conectar();
   $ListaPagoFilas = [];
   $iniciar = ($_GET['pagina']-1)*$filas_por_pagina;
   $Sql_filas ='SELECT *,DATE_FORMAT(Periodo, "%M %Y") AS Periodo,estados.NombreEstado, tipospagos.NombreTipoPago as NTotalPagar, pagos.TotalPagar-SUM(abonos.ValorPago) as NTotalPagar from pagos inner join tipospagos on (pagos.TipoPago = tipospagos.IdTiposPagos) inner JOIN estados on (pagos.IdEstado = estados.IdEstado) LEFT JOIN abonos on (pagos.NCuentaCobro=abonos.IdPago) WHERE abonos.IdPago GROUP BY pagos.NCuentaCobro
   UNION
   SELECT *,DATE_FORMAT(Periodo, "%M %Y") AS Periodo,estados.NombreEstado, tipospagos.NombreTipoPago as NTotalPagar, pagos.TotalPagar-SUM(abonos.ValorPago) as NTotalPagar from pagos inner join tipospagos on (pagos.TipoPago = tipospagos.IdTiposPagos) inner JOIN estados on (pagos.IdEstado = estados.IdEstado) LEFT JOIN abonos on (pagos.NCuentaCobro=abonos.IdPago) WHERE abonos.IdPago IS NULL GROUP BY pagos.NCuentaCobro ORDER BY NCuentaCobro DESC LIMIT :iniciar,:nfilas';
   $sentencia_filas = $Db->query('SET lc_time_names = "es_ES";');
   $sentencia_filas = $Db->prepare($Sql_filas);
   $sentencia_filas->bindParam(':iniciar', $iniciar, PDO::PARAM_INT);
   $sentencia_filas->bindParam(':nfilas', $filas_por_pagina, PDO::PARAM_INT);
   $sentencia_filas->execute();
 
   $resultado_articulos = $sentencia_filas->fetchAll();
   //FIN PAGINACIÓN
        foreach ($resultado_articulos as $Pago)
        {
        $Valor = $Pago["NTotalPagar"];
        $Valor2 = $Pago["TotalPagar"];
        ?>
        <tr>
            
            <td><?php echo $Pago["NCuentaCobro"];?></td><td>
            <?php if($Pago["NombreEstado"]=='Pendiente' ||$Pago["NombreEstado"]=='Plazo vencido' ){
            echo '$' . number_format($Valor2, 2);
           }
           else{echo '$' . number_format($Valor, 2);}?></td>
            <td><?php echo $Pago["NombreTipoPago"];?></td>
            <td align="center"><?php echo $Pago["NApartamento"];?></td>
            <td style="text-transform: capitalize;"><?php echo $Pago["Periodo"];?></td>
            <td><?php echo $Pago["NombreEstado"];?></td>
            <?php
            
            if($Pago["NombreEstado"]=='Pendiente')
            {
              echo '<td align="center"><a type="button" data-toggle="modal" data-target="#dialogo2" data-ncuentacobro="'.$Pago["NCuentaCobro"].'" data-pagocompleto="'.$Pago["TotalPagar"].'"><i class="fas fa-money-check-alt"></i></a>';}
              else if($Pago["NombreEstado"]=='Abonado')
            {
              echo '<td align="center"><a type="button" data-toggle="modal" data-target="#dialogo2" data-ncuentacobro="'.$Pago["NCuentaCobro"].'" data-pagocompleto="'.$Pago["NTotalPagar"].'"><i class="fas fa-money-check-alt"></i></a>';}
              else if($Pago["NombreEstado"]=='Pagado' ||$Pago["NombreEstado"]=='Plazo vencido' )
              {
                echo '<td>';}
              
            ?>
              
            </td>  
          </tr>
        <?php
        
      }
    ?>
  </tbody>
</table>
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">

    <li class="page-item <?php echo($_GET['pagina']<=1 ? 'disabled': '') ?>">
    <a class="page-link" href="ListadoPagos.php?pagina=<?php echo($_GET['pagina']-1); ?>">Anterior</a></li>

    <?php
        for ($i=0; $i < $paginas; $i++):
    ?>
    <li class="page-item <?php echo($_GET['pagina']==$i+1 ? 'active' : '') ?>">
    <a class="page-link" href="ListadoPagos.php?pagina=<?php echo($i+1); ?>"><?php echo($i+1); ?></a></li>
    <?php endfor ?>
    <li class="page-item <?php echo($_GET['pagina']>=$paginas ? 'disabled': '') ?>">
    <a class="page-link" href="ListadoPagos.php?pagina=<?php echo($_GET['pagina']+1); ?>">Siguiente</a></li>
  </ul>
</nav>
</center>
<footer align="center" class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
</body>
<script src="../js/funciones.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>