<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
require_once('../conexion.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <title>Pago adelantado</title>
</head>
<body onload="carg()">
<header>
<div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
</div>
    <center>
        <h1>FORMULARIO PAGO ADELANTADO</h1> <br>
    <form class="form-horizontal" action="../Controlador/ControladorPago.php" method="post" style="align-content: center" id="FrmPagos" name="FrmPagos">
        <div class="form-group">
        <label for="radio" class="control-label col-xs-3">Entidad:</label> 
        <div class="col-xs-2">
        <input type="text" class="form-control" id="Entidad" name="Entidad" value="Av.Villas" disabled > 
        </div>
        <label for="radio" class="control-label col-xs-3">Tipo de cuenta:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="Corriente" class="form-control" id="TipoCuenta" name="TipoCuenta" ">
        </div>
    </div>


    <div class="form-group">
        <label for="" class="control-label col-xs-3">Titular:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="C.R. Altobelo P.H" class="form-control" name="Titular" id="Titular" >
        </div>
        <label for="" class="control-label col-xs-3" id="label1">N° cuenta:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="50317248" class="form-control" name="Ncuenta" id="Ncuenta" >
    </div>
    </div>    
    
    <div class="form-group">
            <label for="" id="label2" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarNcuentaCobro"></label> N° cuenta de cobro:</label> 
            <div class="col-xs-2">
            <input type="text" class="form-control" name="NcuentaCobro" id="NcuentaCobro">
        </div>
        </div>
       
        <div class="form-group">
            <label for="" class="control-label col-xs-3">Fecha:</label>
            <div class="col-xs-2">
            <input type="date" name="FechaActual" id="FechaActual" class="form-control date" >
            </div>  
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarFechaLimite"></label> Fecha límite:</label>
            <div class="col-xs-2">
            <input type="datetime" name="FechaLimite" id="FechaLimite" class="form-control datetime" placeholder="DD-MM-AAAA HH:MM">
        </div>
        </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3"><label for=""  style="color: red;"id="ValidarNapartamento"></label> N° apartamento:</label>
            <div class="col-xs-2">
            <select name="NApartamento" id="NApartamento" onchange="carg()"  class="form-control">
            <option value="0">--Seleccione--</option>
            <?php 
            $Db = Db::Conectar();
            $Sql = $Db->query('SELECT * FROM apartamentos where NApartamento in  (select NApartamento from detallepropietarioapartamento) AND IdEstado=1');
            $Sql->execute();
            $id = $_POST['NApartamento'];
            while($row=$Sql->fetch(PDO::FETCH_ASSOC))
            {
            extract((array)$row);
            ?>
            <option value="<?php echo $row['NApartamento']; ?>"><?php echo $row['NApartamento']; ?></option>
         <?php
            }
            ?>
        </select>
            </div>
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarPropietario"></label>Propietario:</label>
            <div class="col-xs-2">
            <input type="text" name="Propietario"  id="Propietario" readonly class="form-control">
            </div>  
            <input type="hidden" name="TipoPago" id="TipoPago" class="form-control" value=3>
            <input type="hidden" name="IdEstado" id="IdEstado" class="form-control" value=3>
            
            
            
            </div>
            <div class="form-group">
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarDireccionEntrega"></label>Dirección de entrega:</label>
            <div class="col-xs-2">
            <input type="text" name="DireccionEntrega" id="DireccionEntrega" readonly class="form-control">
            </div>
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarEmail"></label>Correo:</label>
            <div class="col-xs-2">
            <input type="text" id="Email" name="Email" readonly class="form-control">
            </div>
            </div>
            <div class="form-group">
            <label class="control-label col-xs-3" for="" id="label3"><label style="color: red;" for="" id="ValidarPeriodoInicio"></label> Periodo Inicio:</label> 
            <div class="col-xs-2">
            <input type="month" class="form-control calendarioI" name="Periodo" val="" placeholder="MM-AAAA"  id="Periodo" onchange="validarFechas()">
            </div>
            <label  class="control-label col-xs-3" for=""><label for="" style="color: red;" id="ValidarPeriodoFin"></label> Periodo Fin:</label> 
            <div class="col-xs-2">
            <input type="month" class="form-control calendarioI" val=""  name="PeriodoFin" id="PeriodoFin" placeholder="MM-AAAA" onchange="validarFechas()">
        </div>
        </div>
            <br><br>
    
        
                    <label for="" id="labelDetalles">Conceptos que intervienen en la cuota de administración</label>
                    
                    <br>
                    <br>
                    <label for="" class="control-label col-xs-3">Concepto</label> 
                    <label for="" class="control-label col-xs-3">Valor</label> 
                    <label for="" class="control-label col-xs-3">Observaciones</label>
                    <br><br>

                    <div class="content">
                    <div class="left">
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarParqueadero"></label>Parqueadero</p> 
                    <div class="midiv col-xs-1">
                    <input class="form-control" name="Parqueadero" readonly id="Parqueadero" value="0" style="text-align:right">
                    </div>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarCuartoutil"></label>Cuarto útil</p> 
                    <div class="midiv col-xs-1">
                    <input for="" class="form-control" name="Cuarto" id="Cuarto" readonly  value="0" style="text-align:right"> 
                    </div>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarM2"></label>M2</p> 
                    <div class="midiv col-xs-1">
                    <input class="form-control" name="M2" value="0" readonly id="M2" style="text-align:right"> 
                    </div>
                    <br><br>
                    <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarPagoTotal"></label> Total a pagar:</label>
                    <div class="midiv col-xs-1">
                    <input type="text" id="TotalPagar" readonly name="TotalPagar" class="form-control" style="text-align:right">
                    </div>
        </div>
        <div class="right">
            <textarea name="Observaciones" class="form-control" id="Observaciones" rows="7"></textarea>
        </div>
        </div>   
                <br><br><br>
                <input type="hidden" name="Registrar" id="Registrar">
                <button type="submit" class="btn btn-success">Registrar</button>
                <a class="btn btn-info" href="ListadoPagos.php?pagina=1">Volver</a>
                
    </form>
    <p align="center" id="RespuestaTransaccion"></p>
    <br><br><br>
    
        </center>
        
</body>
<script src="../js/funciones.js"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>