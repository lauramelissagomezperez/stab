<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
require_once('../conexion.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <title>Residentes</title>
</head>
<body>
<header>
<div class="menu_bar">
      <a href="#" class="btn-menu"><span class="icon-menu"></span>Menu</a>
    </div>
    <div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
    </header>
<h1 align="center">FORMULARIO RESIDENTE</h1> <br><br>
<form  class="form-horizontal" action="../Controlador/ControladorResidente.php" method="post" style="align-content: center" id="FrmResidentes" name="FrmResidentes">
<div class="form-group">
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarNombre"></label>Nombre:</label> 
        <div class="col-xs-2">
        <input id="Nombre" name="Nombre" class="form-control" type="text"> 
        </div>
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarCedula"></label>Cédula:</label> 
        <div class="col-xs-2">
        <input id="Cedula" name="Cedula" class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarTelefono"></label>Teléfono:</label> 
        <div class="col-xs-2">
        <input id="Telefono" name="Telefono" class="form-control" type="text" maxlength="10" >
        </div>

        <label for="radio" class="control-label col-xs-3">Celular:</label> 
        <div class="col-xs-2">
        <input id="Celular" name="Celular" class="form-control" type="text" maxlength="10" >
        </div>

    </div>


    <div class="form-group">
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarCorreo"></label>Correo:</label> 
        <div class="col-xs-2">
        <input id="Correo" name="Correo" class="form-control" type="text">
        </div>

        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarNApartamento"></label> N° Apartamento:</label> 
        <div class="col-xs-2">
        <select name="NApartamento" id="NApartamento" class="form-control">
            <option value="0">--Seleccione--</option>
            <?php 
            $Db = Db::Conectar();
            $Sql = $Db->query('SELECT * FROM apartamentos apto  WHERE apto.NApartamento IN (SELECT pro.NApartamento FROM detallepropietarioapartamento pro) AND IdEstado=1');
            $Sql->execute();
            while($row=$Sql->fetch(PDO::FETCH_ASSOC))
            {
            extract((array)$row);
            ?>
            <option value="<?php echo $row['NApartamento']; ?>"><?php echo $row['NApartamento']; ?></option>
         <?php
            }
            ?>
        </select>
        </div>
    </div>
    <div class="form-group">
    <label for="radio" class="control-label col-xs-3">Observaciones:</label>
        <div class="col-xs-2">
        <textarea id="Observaciones" name="Observaciones" rows="7" class="form-control" type="text"></textarea>
        </div>
    </div>
     <input type="hidden" name="IdEstado" id="IdEstado" class="form-control" value=1>  

    <center>
    <br><br><br>
                <input type="hidden" name="Registrar" id="Registrar">
                <button type="submit" class="btn btn-success">Registrar</button> 
                <button type="button" onclick="Volver()" class="btn btn-info">Volver</button>  
    </center>
    </form>

    <p align="center" id="RespuestaTransaccionResidente"></p>
    <br><br><br>
    <footer align="center" class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
        </center>
        
    
</body>
<script src="../js/funciones.js"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>