<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/styles.css">
        <title>Login</title>
</head>
<body>
    <div class="row" style="width: 100%;">
        <div class="col-md-6">
            <img src="img/login.png" style="height: 91.2vh; width: 100vh;">
        </div>
        <div class="col-md-6 inner-addon left-addon">
            <center>
                <form action="Usuario/Controlador/ControladorUsuario.php" method="POST">
                    <img src="img/logo.jpeg" alt=""><br>
                    <input type="text" id="usuario" name="usuario" class="redondeado" placeholder="Usuario"><i class="fas fa-user" id="icon"></i>
                    <br>
                    <br>
                    <input type="password" name="contrasena" id="contrasena" class="redondeado" placeholder="Contraseña"><i class="fas fa-lock"></i>
                    <br>
                    <br>
                    <button value="Acceder" name="Acceder" id="Acceder">Ingresar</button> 
                </form>
                
            </center>
        </div>
    </div>
    <footer align="center" class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
</body>

<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

</html>